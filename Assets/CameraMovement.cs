﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
	public bool isMoving = true;
	public float movingSpeed = 0.8f;
	[SerializeField] Animator animator;
	[SerializeField] GameObject player;
	public bool isPlayerMoving = true;


	// Use this for initialization
	void Start () {
		isMoving = true;
		animator = GetComponent<Animator>();
		
	}
	
	// Update is called once per frame
	void Update () {
		if(isMoving)
		{
			float targetAngle = Mathf.Atan2(1,1) * Mathf.Rad2Deg;
			//gameObject.transform.Translate(Vector3.down * Time.deltaTime * movingSpeed);
			gameObject.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0,0,targetAngle), movingSpeed * Time.deltaTime);
			//player.transform.Translate(Vector3.forward * Time.deltaTime * movingSpeed);

		}
//
//		if(isPlayerMoving)
//		{
//			player.transform.Translate(Vector3.forward * Time.deltaTime * movingSpeed);
//		}
//
//			
//		if(gameObject.transform.position.x > -10.1 && gameObject.transform.position.x < -9.3 )
//		{
//			isPlayerMoving = false;
//			animator.SetTrigger("isStop");
//		}
//			
//		if(gameObject.transform.position.x > -9.3)
//		{
//			isMoving = false;
//			//animator.SetBool("isStop", true);
//		}
//		
	}
}
