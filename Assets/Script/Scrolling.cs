﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Scrolling : MonoBehaviour {
	[SerializeField] float scrollSpeed = 0.8f;
	bool isScrolling = true;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if(isScrolling)
		{
			gameObject.transform.Translate(Vector3.up * Time.deltaTime * scrollSpeed);
		}

		if(gameObject.transform.position.y > 7)
		{
			gameObject.transform.position =  new Vector3 (0, -9.5f, 0);
		}

		if (Input.GetKeyDown(KeyCode.Escape)) {
			SceneManager.LoadScene("Start");
		}
	}
}
