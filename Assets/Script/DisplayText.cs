﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayText : MonoBehaviour {

	Text levelText;
	LevelManager levelManagerScript;


	// Use this for initialization
	void Start () {
		levelManagerScript = FindObjectOfType<LevelManager>();
		levelText = gameObject.GetComponent<Text>();
		
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log(levelManagerScript.getLevels());
		levelText.text = "Level: "+ levelManagerScript.getLevels().ToString() + " / 3";
		
	}
}
