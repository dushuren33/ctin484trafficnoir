﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerControl : MonoBehaviour {
    const float baseSpeed = 3.0f;
    const float baseReload = 1.0f;
    const float baseDamage = 5.0f;
    const float baseHealth = 20.0f;
    public float movespeed;
    public float health;
    private Rigidbody myRigidBody;
    private Vector3 moveInput;
    private Vector3 moveVelocity;
    private Camera mainCamera;
    public GunController Gun;
    public AmmoManager ammo;
    public Text AmmoCount;
	public float delay=3.0f;
    public AudioClip gs;
    public AudioClip gs1;
    public AudioClip gs2;
    public AudioClip gs3;
    public AudioClip gs4;
    public AudioClip scream;
    [SerializeField]Text HP;
	[SerializeField]Animator animator;
	// Use this for initialization
	void Start () {

        health = baseHealth;
        myRigidBody = GetComponent<Rigidbody>();
        mainCamera = FindObjectOfType<Camera>();
        ammo = this.GetComponent<AmmoManager>();
        AmmoCount.text = "Ammunation: " + ammo.ammo.ToString() + "/" + ammo.maxAmmo.ToString();
		HP.text = "Health: " + health.ToString() + "/20";
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = gs;
        if(FindObjectOfType<LevelManager>().MachineGun)
        {
            Gun.damage = 5;
            Gun.timeBetweenShots = 0.1f;
            Gun.bulletSpeed = 8;
            ammo.maxAmmo = 100;
            ammo.ammo = 100;
        }
        if (FindObjectOfType<LevelManager>().fastWalk)
        {
            movespeed = 8;
        }
        if (FindObjectOfType<LevelManager>().bulletProof)
        {
            health = 100;
            HP.text = "Health: " + health.ToString() + "/100";
        }
    }
	
	// Update is called once per frame
	void Update () {


        if (Mathf.Abs(transform.position.y - 0.695f) > 0.2f)
            transform.position.Set(transform.position.x, 0.695f, transform.position.z);

        movespeed = baseSpeed + FindObjectOfType<LevelManager>().speedLvlup;
        Gun.damage = baseDamage + FindObjectOfType<LevelManager>().damageLvlup;
        Gun.timeBetweenShots = (baseReload * 5) / (5 + (float)FindObjectOfType<LevelManager>().ShootingSpeedLvlup);
        if ( FindObjectOfType<EnemyController>()== null)
			SceneManager.LoadScene("Traffic");
		//Debug.Log(health);
		HP.text = "Health: " + health.ToString() + "/20";
        if (FindObjectOfType<LevelManager>().bulletProof)
        {
            HP.text = "Health: " + health.ToString() + "/100";
        }
		moveInput = new Vector3(Input.GetAxisRaw("Horizontal")*1.3f, 0f, Input.GetAxisRaw("Vertical")*1.0f);
        moveVelocity = moveInput * movespeed;
        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLength;
        if (groundPlane.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLength);

            transform.LookAt(new Vector3(pointToLook.x, transform.position.y,pointToLook.z));
           
        }
		//Debug.Log(Input.anyKeyDown);
        if (Input.GetAxisRaw("Vertical") != 0)
		{
			animator.SetBool("isRunning", true);	
		}
		else
		{
			//animator.SetBool("isRunning", false);
		}


        if(Input.GetMouseButtonDown(0))
        {
            Gun.isFiring = true;
            Random rand = new Random();
            float x = (int)Random.value * 6;
            if( x==0)
                GetComponent<AudioSource>().clip = gs;
            else if (x==1)
                GetComponent<AudioSource>().clip = gs1;
            else if (x == 2)
                GetComponent<AudioSource>().clip = gs2;
            else if (x == 3)
                GetComponent<AudioSource>().clip = gs3;
            else 
                GetComponent<AudioSource>().clip = gs4;
            GetComponent<AudioSource>().Play();
			animator.SetTrigger("isFiring");
        }
        if (Input.GetMouseButtonUp(0))
        {
            Gun.isFiring = false;
        }
       AmmoCount.text = "Ammunation: " + ammo.ammo.ToString() + "/" + ammo.maxAmmo.ToString();

       
    }
    void takeDamage(float damage)
    {
        GetComponent<AudioSource>().clip = scream;
        GetComponent<AudioSource>().Play();
        health -= damage;
		health = Mathf.Clamp(health, 0, 20);
        if (health <= 0)
		{

            Destroy(Gun.gameObject);
			animator.SetTrigger("isDead");
			//Destroy(this.gameObject, 3);

			StartCoroutine(LoadLoseScene(delay));
			//SceneManager.LoadScene("Lose");
			
		}
    }

	IEnumerator LoadLoseScene(float delay)
	{
		yield return new WaitForSeconds(delay);
		SceneManager.LoadScene("Lose");

	}

    void FixedUpdate()
    {
		myRigidBody.velocity = this.transform.rotation*moveVelocity;
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ammo")
        {
            Destroy(other.gameObject);
                
            ammo.pickup(999);
        }
        if (other.gameObject.tag=="Bullet")
        {

            takeDamage((other.gameObject.GetComponent<BulletController>()).damage);
            Destroy(other.gameObject);
        }
        if (other.gameObject.tag=="ShooterCar")
        {
            takeDamage(10);
            Destroy(other.gameObject);
        }
        
    }



}
