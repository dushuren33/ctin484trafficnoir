﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoManager : MonoBehaviour {
    public int ammo;
    public int maxAmmo;
	// Use this for initialization
	void Start () {
        ammo = maxAmmo;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    int remaining()
    {
        return Mathf.Max(ammo,0);
    }
    public bool firing()
    {
        if (ammo > 0)
        { 
            ammo--;
            return true;
        }
        else return false;
    }
    public void pickup(int num)
    {
        ammo = Mathf.Min(num,maxAmmo);
    }

}
