﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {
    public float Speed;
    public float life;
    public float damage;
	// Use this for initialization
	void Start () {
       
        Destroy(this.gameObject, life);
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.forward * Speed * Time.deltaTime);
        //transform.rotation = transform.localRotation;
	}
    void OnCollisionEnter(Collision other)
    {

        if(other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<EnemyController>().takeDamage(damage);
        }
       if (other.gameObject.tag=="ShooterCar"||other.gameObject.tag=="Enemy") {
            Destroy(this.gameObject);
            Debug.Log("hit car");
        }
        Debug.Log("hit car");
        Destroy(this.gameObject);
    }
}
