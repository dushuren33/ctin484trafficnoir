﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour {
    public float damage;
    public bool isFiring;
    public BulletController bullet;
    public float bulletSpeed;
    public float timeBetweenShots;
    public float shotCounter;
    public Transform firePoint;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(isFiring)
        {
            shotCounter -= Time.deltaTime;
            
            if(shotCounter<=0 )//&& transform.parent.gameObject.GetComponent<AmmoManager>().firing())
            {

              
                shotCounter = timeBetweenShots;
                BulletController newBullet = Instantiate(bullet, firePoint.position, firePoint.rotation) as BulletController;
                newBullet.Speed = bulletSpeed;
                newBullet.damage = damage;

                if(transform.parent.gameObject.tag=="Player")
                transform.parent.gameObject.GetComponent<AmmoManager>().firing();
            }
            else if (shotCounter <= 0 && transform.parent.gameObject.tag=="Enemy")
            {
                shotCounter = timeBetweenShots;
                BulletController newBullet = Instantiate(bullet, firePoint.position, firePoint.rotation) as BulletController;
                newBullet.transform.rotation = transform.rotation;
                newBullet.Speed = bulletSpeed;
                newBullet.damage = damage;
            }
                
        }
        else
        {
            shotCounter -= Time.deltaTime;
            // shotCounter = 0;
        }
	}
}
