﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyController : MonoBehaviour {
    public GunController theGun;
    public PlayerControl thePlayer;
    public float movespeed;
    public float health;
    public LevelUpScript lev;
    public AmmoCube ammobox;
    private Rigidbody myRigidBody;
    private Vector3 moveInput;
    private Vector3 moveVelocity;
    public float targetTime = 3.0f;
    private float moveTimer;
    // Use this for initialization
    void Start() {
        myRigidBody = GetComponent<Rigidbody>();
        thePlayer = FindObjectOfType<PlayerControl>();
        if (health<=5.0f) health = 5.0f;
         moveTimer = 1.0f;
        
    }

    // Update is called once per frame
    void Update() {

        if (Mathf.Abs(transform.position.y - 0.695f) > 0.2f)
            transform.position.Set(transform.position.x, 0.695f, transform.position.z);
        moveTimer -= Time.deltaTime;
        moveTimer += Time.deltaTime * Random.value;
        if (moveTimer <= 0)
        {
            Vector3 toplayer = thePlayer.transform.position - this.transform.position;
            toplayer = Vector3.Normalize(toplayer);
            
            Random rand = new Random();
            float x = Random.value - 0.5f;
            float z = Random.value - 0.5f;
            moveInput = new Vector3(x /2+toplayer.x/10, 0f, z/2 + toplayer.z/10);
           
            moveTimer = targetTime;
            theGun.isFiring = true;
        }
        //else
            //theGun.isFiring = false;
        transform.LookAt(thePlayer.transform.position);
        
    }
    public void takeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        { Destroy(this.gameObject);
            Random rand = new Random();
           if (Random.value < 0.75)
            {
                AmmoCube newAmmo = Instantiate(ammobox, this.gameObject.transform.position, this.gameObject.transform.rotation) as AmmoCube;
            }
            else
            {   LevelUpScript newLevelUp = Instantiate(lev, this.gameObject.transform.position, this.gameObject.transform.rotation) as LevelUpScript; }
        
    }
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ShooterCar")
            Destroy(other.gameObject);
        
    }
    void FixedUpdate()
    {//
        moveVelocity = moveInput * movespeed;
        myRigidBody.velocity = moveVelocity;
    }
}
