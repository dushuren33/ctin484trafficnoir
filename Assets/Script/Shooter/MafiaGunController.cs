﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MafiaGunController : GunController
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isFiring)
        {
            shotCounter -= Time.deltaTime;
            if (shotCounter <= 0)
            {


                shotCounter = timeBetweenShots;
                BulletController newBullet = Instantiate(bullet, firePoint.position, firePoint.rotation) as BulletController;
                newBullet.Speed = bulletSpeed;
                newBullet.damage = damage;

            }
        }
        else
        {
            shotCounter -= Time.deltaTime;
            // shotCounter = 0;
        }
    }
}
