﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BossController : MonoBehaviour {
    public GunController theGun;
    public PlayerControl thePlayer;
    public float movespeed;
    public float health;
   
    public AmmoCube ammobox;
    private Rigidbody myRigidBody;
    private Vector3 moveInput;
    private Vector3 moveVelocity;
    public float targetTime = 3.0f;
    private float moveTimer;
    // Use this for initialization
    void Start() {
        myRigidBody = GetComponent<Rigidbody>();
        thePlayer = FindObjectOfType<PlayerControl>();
        health = 5.0f;
         moveTimer = 1.0f;
        
    }

    // Update is called once per frame
    void Update() {
        theGun.isFiring = true;
        moveTimer -= Time.deltaTime;
        moveTimer += Time.deltaTime * Random.value;
       
       
        transform.LookAt(thePlayer.transform.position);
        
    }
    public void takeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        { Destroy(this.gameObject);
            AmmoCube newAmmo = Instantiate(ammobox, this.gameObject.transform.position, this.gameObject.transform.rotation) as AmmoCube;
        }
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ShooterCar")
            Destroy(other.gameObject);
        takeDamage(1);
    }
    void FixedUpdate()
    {//
        moveVelocity = moveInput * movespeed;
        myRigidBody.velocity = moveVelocity;
    }
}
