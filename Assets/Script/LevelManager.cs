﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {
    public int levels;
    public Vector3 CarLoc;
    public bool MachineGun=false;
    public bool fastWalk=false;
    public bool bulletProof=false;
    public int ShootingSpeedLvlup = 0;
    public int damageLvlup = 0;
    public int hpLvlup = 0;
    public int speedLvlup = 0;
    // Use this for initialization
    void Start () {
        CarLoc = new Vector3(-29.3f, 0.0f, -12.1f);
        DontDestroyOnLoad(transform.gameObject);
        levels = 0;
       
       
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.P))
		{
			MachineGun=true;
			fastWalk=true;
			bulletProof=true;
		}
	}
    public int getLevels()
    {
        return levels;
    }
    public void setLevels(int mlvl)
    {
        levels = mlvl;
    }
}
