﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUpScript : MonoBehaviour {
    public enum type { Reload, speed, damage };
    public type typ;
    public int magitude;
	// Use this for initialization
	void Start () {
        Random rand = new Random();
        typ = (type)(int)(Random.value * 3);
        magitude= (int)(Random.value * 3);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("touched player");
            if (typ == type.Reload)
                FindObjectOfType<LevelManager>().ShootingSpeedLvlup++;
            else if (typ == type.speed)
                FindObjectOfType<LevelManager>().speedLvlup++;
            else
                FindObjectOfType<LevelManager>().damageLvlup++;
        }
        Destroy(this.gameObject);
    }
}
