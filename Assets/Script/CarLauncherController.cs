﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarLauncherController : MonoBehaviour {
    public bool GreenLit;
    public CarController Car;
    public float CarSpeed;
    public float timeBetweenCars;
    public float CarCounter;
    public Transform firePoint;
    // Use this for initialization
    void Start () {
        GreenLit = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (GreenLit)
        {
            CarCounter -= Time.deltaTime;
            if (CarCounter <= 0)
            {
                CarCounter = timeBetweenCars;
                CarController newBullet = Instantiate(Car, firePoint.position, firePoint.rotation) as CarController;
                newBullet.Speed = CarSpeed;

            }
        }
        else
        {
            CarCounter = 0;
        }
    }
}
