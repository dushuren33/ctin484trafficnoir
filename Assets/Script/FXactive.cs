﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXactive : MonoBehaviour {
	//[SerializeField] float TimeDelayToReactivate = 3f;


	// Use this for initialization
	void Start () {
		InvokeRepeating("Reactive", Random.Range(4f,7f), Random.Range(3f,5f));

		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void Reactive()
	{
		gameObject.SetActive(false);
		gameObject.SetActive(true);
	}
}
