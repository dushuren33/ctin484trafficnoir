﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneController: MonoBehaviour {
	GameObject backButton;
	GameObject InstrctuonBoard;

	// Use this for initialization
	void Start () {
		backButton = GameObject.Find("Back");
		InstrctuonBoard = GameObject.Find("INS");
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LaunchTrafficScene()
	{
		SceneManager.LoadScene("Traffic");
	}

	public void LaunchCreditScene()
	{
		SceneManager.LoadScene("Credits");
	}

	public void LauchStartScene()
	{
		SceneManager.LoadScene("Start");
        FindObjectOfType<LevelManager>().setLevels(0);

    }

	public void Quit()
	{
		Application.Quit();
	}


	public void LaunchInstrucScene()
	{
		SceneManager.LoadScene("Instruc");
	}
}
