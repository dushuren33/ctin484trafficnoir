﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class REactivator : MonoBehaviour {
	//[SerializeField] float TimeDelayToReactivate = 1f;
	MeshRenderer mRenderer;

	// Use this for initialization
	void Start () {
		mRenderer = gameObject.GetComponent<MeshRenderer>();
		StartCoroutine("Reactivate");
		
	}
	
	// Update is called once per frame
	void Update () {
		StartCoroutine("Reactivate");
		
	}

	IEnumerator Reactivate()
	{
//		gameObject.SetActive(false);
//		yield return new WaitForSeconds(TimeDelayToReactivate);
//		gameObject.SetActive(true);
//		yield return new WaitForSeconds(TimeDelayToReactivate);
		mRenderer.enabled = false;
		yield return new WaitForSeconds(1f);
		mRenderer.enabled = true;
		yield return new WaitForSeconds(1f);
	}
}
