﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class DriveAbleScript : MonoBehaviour
{
    public float movespeed;
    public float maximumspeed;
    private Rigidbody myRigidBody;
    private Vector3 moveInput;
    private Vector3 moveVelocity;
    private Vector3 DrivingDirection;
    public GameObject crashFx;

    [SerializeField]
    Text HP;
    // Use this for initialization
    void Start()
    {

     
        myRigidBody = GetComponent<Rigidbody>();
        movespeed = 1.0f;
        DrivingDirection = new Vector3(1, 0, 0);
        transform.position = FindObjectOfType<LevelManager>().CarLoc;
    }
    IEnumerator CrashDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        RestartCar();

    }
    IEnumerator UpgradeDisplay(float delay)
    {
        yield return new WaitForSeconds(delay);
        FindObjectOfType<UpgradeText>().gameObject.SetActive(false);

    }
    void RestartCar()
    {
        movespeed = 0;
        moveVelocity = DrivingDirection * movespeed;
        myRigidBody.velocity = Vector3.zero;
        myRigidBody.angularVelocity = Vector3.zero;
        Random rand = new Random();

        
            transform.position = new Vector3(-25.3f, 0.0f, -12.1f);
            myRigidBody.rotation = Quaternion.LookRotation( new Vector3(0.0f, 0.0f, -12.1f)-transform.position, new Vector3(0.0f, 0.0f, -12.1f) - transform.position);
            
       

    }
    // Update is called once per frame
    void Update()
    {
        movespeed += Input.GetAxisRaw("Vertical") * Time.deltaTime*Mathf.Pow(maximumspeed- Mathf.Abs(movespeed),2)/maximumspeed;
        if (Input.GetAxisRaw("Vertical") != 1)
            movespeed -= Time.deltaTime*movespeed*2/10;
        if (Input.GetAxisRaw("Vertical")==-1)
            movespeed -= Time.deltaTime * movespeed / 2;
        DrivingDirection = Quaternion.AngleAxis(-45*Time.deltaTime* -Input.GetAxisRaw("Horizontal"), Vector3.up) * DrivingDirection;
       
        moveVelocity = DrivingDirection * movespeed;
        
      


    }
    void takeDamage(float damage)
    {
        
    }
    void FixedUpdate()
    {
        myRigidBody.velocity = moveVelocity;
        myRigidBody.rotation = Quaternion.LookRotation(DrivingDirection, DrivingDirection);
    }
    void OnCollisionEnter(Collision other)
    {
        if ((other.gameObject.tag == "Deadly"))
        {
            StartCoroutine(CrashDelay(1.0f));
            Instantiate(crashFx, myRigidBody.position, myRigidBody.rotation) ;
           
           // SceneManager.LoadScene("Lose");
        }
        if ((other.gameObject.tag == "Cross3") && ((FindObjectOfType<LevelManager>().getLevels() == 0)))
        {
            FindObjectOfType<LevelManager>().CarLoc = transform.position;
            SceneManager.LoadScene("Shooter");
            FindObjectOfType<LevelManager>().setLevels(1);
        }
        if ((other.gameObject.tag == "cross4") && ((FindObjectOfType<LevelManager>().getLevels() == 1)))
        {
            FindObjectOfType<LevelManager>().CarLoc = transform.position;
            SceneManager.LoadScene("Shooter2");
            FindObjectOfType<LevelManager>().setLevels(2);
        }
        if ((other.gameObject.tag == "LootCross") && ((FindObjectOfType<LevelManager>().getLevels() == 2)))
        {
            FindObjectOfType<LevelManager>().MachineGun = true;
            FindObjectOfType<LevelManager>().setLevels(3);
            FindObjectOfType<UpgradeText>().gameObject.SetActive(true);
            StartCoroutine(UpgradeDisplay(3.0f));

        }
        if ((other.gameObject.tag == "LootCross1") && ((FindObjectOfType<LevelManager>().getLevels() == 2)))
        {
            FindObjectOfType<LevelManager>().bulletProof = true;
            FindObjectOfType<LevelManager>().setLevels(3);
            FindObjectOfType<UpgradeText>().gameObject.SetActive(true);
            StartCoroutine(UpgradeDisplay(3.0f));
        }
        if ((other.gameObject.tag == "LootCross2") && ((FindObjectOfType<LevelManager>().getLevels() == 2)))
        {
            FindObjectOfType<LevelManager>().fastWalk = true;
            FindObjectOfType<LevelManager>().setLevels(3);
            FindObjectOfType<UpgradeText>().gameObject.SetActive(true);
            StartCoroutine(UpgradeDisplay(3.0f));
        }
        if ((other.gameObject.tag == "cross5") && ((FindObjectOfType<LevelManager>().getLevels() == 3)))
        {
            SceneManager.LoadScene("Boss");
            FindObjectOfType<LevelManager>().setLevels(4);
        }
        RestartCar();
    }


}
