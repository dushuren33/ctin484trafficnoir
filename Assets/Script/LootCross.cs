﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LootCross : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        if (FindObjectOfType<LevelManager>().getLevels() >= 3)
        {
            GetComponent<Collider>().isTrigger = true;
            Component[] Things;
            Things = this.GetComponentsInChildren<Component>();
            foreach (Component obj in Things)
            {
                // Debug.Log(obj.name);
                if (obj.tag == ("Destroy"))
                    obj.gameObject.SetActive(false);
                else obj.gameObject.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
		if (FindObjectOfType<LevelManager>().getLevels() >= 3)
		{
			GetComponent<Collider>().isTrigger = true;
			Component[] Things;
			Things = this.GetComponentsInChildren<Component>();
			foreach (Component obj in Things)
			{
				// Debug.Log(obj.name);
				if (obj.tag == ("Destroy"))
					obj.gameObject.SetActive(false);
				else obj.gameObject.SetActive(true);
			}
		}
	}
    
    void OntriggerEnter(Collision other)

    {
        FindObjectOfType<LevelManager>().MachineGun = true;
    }
}