﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Cross3 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        if (FindObjectOfType<LevelManager>().getLevels() != 0)
        {
            GetComponent<Collider>().isTrigger = true;

            Component[] Things;
             Things= this.GetComponentsInChildren<Component>();
            foreach(Component obj in Things)
            {
               // Debug.Log(obj.name);
               if (obj.tag == ("Destroy"))
                    obj.gameObject.SetActive(false);
               else obj.gameObject.SetActive(true);
            }
        }
             


    }

    // Update is called once per frame
    void Update()
    {

    }
    void OntriggerEnter(Collision other)

    {
    }
}